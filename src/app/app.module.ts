import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { BLE } from '@ionic-native/ble';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { DisplayTemperaturePage } from '../pages/display-temperature/display-temperature';
import { TossCupPage } from '../pages/toss-cup/toss-cup';
import { MainLobbyPage } from '../pages/main-lobby/main-lobby';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    DisplayTemperaturePage,
    TossCupPage,
    MainLobbyPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    DisplayTemperaturePage,
    TossCupPage,
    MainLobbyPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    BLE,
  ]
})
export class AppModule {}
