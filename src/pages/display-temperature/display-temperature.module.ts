import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {DisplayTemperaturePage} from './display-temperature';
import {BLE} from "@ionic-native/ble";

@NgModule({
  declarations: [
    DisplayTemperaturePage,
  ],
  imports: [
    IonicPageModule.forChild(DisplayTemperaturePage),
  ],
  providers: [
    BLE
  ]
})
export class DisplayTemperaturePageModule {}
