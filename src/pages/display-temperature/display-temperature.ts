import {Component, NgZone} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BLE } from '@ionic-native/ble';

/**
 * Generated class for the DisplayTemperaturePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-display-temperature',
  templateUrl: 'display-temperature.html',
})
export class DisplayTemperaturePage {
  private SERVICE_UUID = '6E400001-B5A3-F393-E0A9-E50E24DCCA9E';
  private NOTIFICATION_UUID = '6E400003-B5A3-F393-E0A9-E50E24DCCA9E';
  private readonly device;
  private temp = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, private ble: BLE, private ngZone: NgZone) {
    this.device = this.navParams.get('peripheral');
    console.log(this.device);
    this.readTemperature();
  }

  readTemperature() {
    this.ble.startNotification(this.device.id, this.SERVICE_UUID, this.NOTIFICATION_UUID).subscribe(buffer => {
      const data = new Float32Array(buffer);

      console.log('Received notif');
      console.log(data[0]);
      this.ngZone.run(() => {
        this.temp = data[0];
      })
    }, console.log);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DisplayTemperaturePage');
  }

}
