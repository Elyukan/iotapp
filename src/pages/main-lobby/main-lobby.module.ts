import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainLobbyPage } from './main-lobby';

@NgModule({
  declarations: [
    MainLobbyPage,
  ],
  imports: [
    IonicPageModule.forChild(MainLobbyPage),
  ],
})
export class MainLobbyPageModule {}
