import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BLE } from '@ionic-native/ble';
// import { DisplayTemperaturePage } from '../display-temperature/display-temperature';
// import { TossCupPage } from '../toss-cup/toss-cup';

/**
 * Generated class for the MainLobbyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-main-lobby',
  templateUrl: 'main-lobby.html',
})
export class MainLobbyPage {
  private SERVICE_UUID = '6E400001-B5A3-F393-E0A9-E50E24DCCA9E';
  private CONTROLLER_UUID = '6E400002-B5A3-F393-E0A9-E50E24DCCA9E';
  private NOTIFICATION_UUID = '6E400003-B5A3-F393-E0A9-E50E24DCCA9E';
  private temp = 0;
  device: any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, private ble: BLE, private ngZone: NgZone) {
    this.device = this.navParams.get('device');
    this.readTemperature();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainLobbyPage');
  }

  ionViewWillLeave() {
    console.log('ionViewWillLeave disconnecting Bluetooth');
    this.ble.disconnect(this.device.id).then(
      () => console.log('Disconnected ' + JSON.stringify(this.device)),
      () => console.log('ERROR disconnecting ' + JSON.stringify(this.device))
    )
  }

  readTemperature() {
    this.ble.startNotification(this.device.id, this.SERVICE_UUID, this.NOTIFICATION_UUID).subscribe(buffer => {
      const data = new Float32Array(buffer);

      console.log('Received notif');
      console.log(data[0]);
      this.ngZone.run(() => {
        this.temp = data[0];
      })
    }, console.log);
  }

  str2ab(str) {
    console.log(str)
    var buf = new ArrayBuffer(str.length * 2); // 2 bytes for each char
    var bufView = new Uint16Array(buf);
    for (var i = 0, strLen = str.length; i < strLen; i++) {
      bufView[i] = str.charCodeAt(i);
    }
    console.log(JSON.stringify(buf))
    return buf;
  }


  activateToss() {
    this.ble.writeWithoutResponse(this.device.id, this.SERVICE_UUID, this.CONTROLLER_UUID, this.str2ab('1')).then(() => console.log('Msg sent')).catch((err) => console.log(err));
  }

  desactivateToss() {
    this.ble.writeWithoutResponse(this.device.id, this.SERVICE_UUID, this.CONTROLLER_UUID, this.str2ab('0')).then(() => console.log('Msg sent')).catch((err) => console.log(err));
  }
}
