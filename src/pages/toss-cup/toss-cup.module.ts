import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TossCupPage } from './toss-cup';

@NgModule({
  declarations: [
    TossCupPage,
  ],
  imports: [
    IonicPageModule.forChild(TossCupPage),
  ],
})
export class TossCupPageModule {}
