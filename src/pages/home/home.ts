import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MainLobbyPage } from '../main-lobby/main-lobby';
import { BLE } from '@ionic-native/ble';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  device: any = '';

  constructor(public navCtrl: NavController, private ble: BLE) {
    this.ble.isConnected(this.device.id).then(() => this.onGoToMainLobby(this.device)).catch(() => this.statusReport("not connected"));
  }

    onGoToConnection() {
      this.ble.scan([], 10).subscribe(device => {
        if (device.name === "TTime") {
          this.ble.connect(device.id).subscribe(
            () => this.onGoToMainLobby(device)
          );
        }
      });
    }

    statusReport(message) {
      console.log(message);
    }

    onGoToMainLobby(device) {
      console.log("success");
      this.device = device;
      this.navCtrl.push(MainLobbyPage, {
        device: device
      });
    }

}
